package com.example.rsocketwebserver.data.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue
    private String id;

    private String username;

    protected User(){}

    public User(String username){
        this.username = username;
    }

    public void setUser(String username) {this.username = username; }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void changeUsername(String username) {
        this.username = username;
    }
}
