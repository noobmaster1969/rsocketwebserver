package com.example.rsocketwebserver.data.entity;

import javax.persistence.Entity;

@Entity
public class GroupChat extends DMChat {

    private String GroupName;

    private String GroupPic;

    protected GroupChat(){}

    public GroupChat(String GroupName ,String GroupPic, User creator){

        setChatType(ChatType.GROUP);
        this.GroupName = GroupName;
        this.GroupPic = GroupPic;
    }

    public String getGroupName() {
        return GroupName;
    }

    public void setGroupName(String groupName) {
        GroupName = groupName;
    }

    public String getGroupPic() {
        return GroupPic;
    }

    public void setGroupPic(String groupPic) {
        GroupPic = groupPic;
    }
}
