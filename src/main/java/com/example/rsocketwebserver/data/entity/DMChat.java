package com.example.rsocketwebserver.data.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class DMChat {

    @Id
    @GeneratedValue
    private String id;

    @OneToMany
    private List<ChatMessage> chatMessages;

    @OneToMany
    private List<User> users;

    private ChatType chatType;

    public String getId() {
        return id;
    }

    protected DMChat(){}

    public DMChat(List<User> Users){
        setChatType(ChatType.DM);
        setUsers(Users);
    }

    public List<ChatMessage> getChatMessages() {
        return chatMessages;
    }
    public void setChatMessages(List<ChatMessage> chatMessages) {
        this.chatMessages = chatMessages;
    }

    public List<User> getUsers() {
        return users;
    }
    public void setUsers(List<User> users) {
        this.users = users;
    }
    public void addUser(User newUser){this.users.add(newUser);}
    public void removeUser(User user){this.users.remove(user);}

    public ChatType getMessageType() {
        return chatType;
    }
    public void setChatType(ChatType chatType) {
        this.chatType = chatType;
    }
}
