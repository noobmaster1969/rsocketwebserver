package com.example.rsocketwebserver.data.entity;

public enum ChatType {
    DM,
    GROUP
}
