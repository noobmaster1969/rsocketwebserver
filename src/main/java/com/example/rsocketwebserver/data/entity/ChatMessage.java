package com.example.rsocketwebserver.data.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ChatMessage {

    @Id
    @GeneratedValue
    private String id;

    @ManyToOne
    private DMChat DMChat;

    private String message;

    protected ChatMessage(){}

    public ChatMessage(String message, DMChat DMChat){
        this.message = message;
        this.DMChat = DMChat;
    }

    public String getId() {
        return id;
    }

    public com.example.rsocketwebserver.data.entity.DMChat getDMChat() {
        return DMChat;
    }

    public void setDMChat(com.example.rsocketwebserver.data.entity.DMChat DMChat) {
        this.DMChat = DMChat;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
