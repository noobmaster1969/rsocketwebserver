package com.example.rsocketwebserver.presentation;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route
public class MainView extends VerticalLayout {
    public MainView(){
        add(new Button("CLICK ME", e -> Notification.show("Hello vaadin user")));
    }
}
